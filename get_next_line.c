/*
** get_next_line.c for get_next_line in
** /home/dupic_b/Documents/Project/get_next_line
** 
** Made by dupic_b
** Login   <dupic_b@epitech.net>
** 
** Started on  Mon Nov 18 11:09:49 2013 dupic_b
** Last update Mon Nov 18 21:10:55 2013 dupic_b
*/

#include "get_next_line.h"

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  while (i < n && src[i] != '\0')
    {
      dest[i] = src[i];
      i = i + 1;
    }
  dest[i] = '\0';
  return (dest);
}

char	*my_strcat(char *dest, char *src)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (dest[i] != '\0')
    i = i + 1;
  while (src[j] != '\0')
    {
      dest[i] = src[j];
      i = i + 1;
      j = j + 1;
    }
  dest[i] = '\0';
  return (dest);
}

char	*my_realloc(char *str, int new_size)
{
  int	len;
  char	*new_str;

  len = 0;
  while (str[len])
    len++;
  if (new_size <= len)
    return (NULL);
  if ((new_str = malloc(sizeof(char) * new_size)) == NULL)
    return (NULL);
  new_str = my_strncpy(new_str, str, len);
  free(str);
  return (new_str);
}

char	*fill_buffer(const int fd, char *stock, int *loop)
{
  int	ret;
  char	buffer[READ_SIZE + 1];

  if ((stock = malloc(sizeof(char) * (READ_SIZE + 1))) == NULL)
    return (NULL);
  stock[0] = 0;
  while ((ret = read(fd, buffer, READ_SIZE)) > 0)
    {
      buffer[ret] = 0;
      if ((stock = my_realloc(stock, (READ_SIZE * *loop + 1))) == NULL)
	return (NULL);
      stock = my_strcat(stock, buffer);
      *loop = *loop + 1;
    }
  if (ret == -1)
    return (NULL);
  return (stock);
}

char		*get_next_line(const int fd)
{
  static char	*stock = NULL;
  static int	loop = 1;
  char		*retour;
  int		i;

  i = 0;
  retour = NULL;
  if (loop == 1)
    if ((stock = fill_buffer(fd, stock, &loop)) == NULL)
      return (NULL);
  while (*stock != '\n' && *stock != '\0')
    {
      i++;
      stock++;
    }
  if ((retour = malloc(sizeof(char) * (i + 1))) == NULL)
    return (NULL);
  if (i == 0 && *stock == '\0')
    return (NULL);
  stock = stock - i;
  retour = my_strncpy(retour, stock, i);
  stock = stock + i + 1;
  return (retour);
}
