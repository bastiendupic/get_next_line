/*
** get_next_line.h for get_next_line in
** /home/dupic_b/Documents/Project/get_next_line
** 
** Made by dupic_b
** Login   <dupic_b@epitech.net>
** 
** Started on  Mon Nov 18 11:11:01 2013 dupic_b
** Last update Mon Nov 18 14:51:26 2013 dupic_b
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_
# define READ_SIZE 1024
# include <stdlib.h>
# include <unistd.h>

char	*get_next_line(const int);

#endif /* !GET_NEXT_LINE_H_ */
